# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 23:15:33 2020

@author: blazt_000
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn

#3. PRZYGOTOWANIE DANYCH

games = pd.read_csv("steam.csv")

games = games[games.english !=0]
games = games.drop('english', 'columns')

games['owners'] = pd.to_numeric([i.split("-")[0] for i in games['owners']])

income = games['owners']*games['price']
games['income'] = income

games = games.drop(labels=['owners','price','release_date','developer', 
                           'publisher','platforms','required_age','median_playtime',
'average_playtime','negative_ratings','positive_ratings',
'achievements','categories','genres','name','steamspy_tags'],
axis = 'columns')

games = games[games.income != 0]

tags = pd.read_csv("steamspy_tag_data.csv")

tags.drop([col for col, val in tags.sum().iteritems() if val < 1000], axis=1, inplace=True)

zbior = pd.merge(tags, games, on='appid', how='inner')
zbior = zbior.drop('appid', 'columns')

# print(zbior.info())

#4. ANALIZA DANYCH

#Tworzymy kategorię porażka-sukces w zmiennej income
zbior["success"] = pd.cut(zbior["income"], [0, 1000000, 100000000000], right=False, labels=["porazka", "sukces"])
print(zbior["success"].value_counts())

#bierzemy do zbioru zmiennych opisujacych X nasze tagi, do zbioru zmiennych celu X income
X = zbior.iloc[:, 0:320]
y = zbior["success"]
#tworzymy yk, ktore zawiera kategorie 0-1, 0 dla porazki, 1 dla sukcesu
yk = y.cat.codes.values

import sklearn.model_selection
idx_ucz, idx_test = sklearn.model_selection.train_test_split(np.arange(X.shape[0]), test_size=0.2, random_state=12345)
X_ucz, X_test = X.iloc[idx_ucz, :], X.iloc[idx_test, :]
y_ucz, y_test = y[idx_ucz], y[idx_test]
yk_ucz, yk_test = yk[idx_ucz], yk[idx_test]
# print(X_ucz.shape, X_test.shape, y_ucz.shape, y_test.shape) #ksztalty sie zgadzaja


from sklearn import preprocessing
#normalizujemy wektory l2
X_norm_l2_ucz = preprocessing.normalize(X_ucz)
X_norm_l2_test = preprocessing.normalize(X_test)
X_norm_l2 = preprocessing.normalize(X)
#normalizujemy wektory l1
X_norm_l1_ucz = preprocessing.normalize(X_ucz, norm = "l1")
X_norm_l1_test = preprocessing.normalize(X_test, norm = "l1")
X_norm_l1 = preprocessing.normalize(X, norm = "l1")


#5. MODELOWANIE DANYCH

# #metoda k-najbliższych sąsiadów
import sklearn.neighbors
knn = sklearn.neighbors.KNeighborsClassifier()
yk_pred = knn.fit(X_ucz, yk_ucz)
yk_pred = knn.predict(X_test)
y_pred = y.cat.categories[yk_pred]
#Ocena jakosci klasyfikatora

import sklearn.metrics
#przykladowe miary oceny jakości klasyfikatora: precyzja - precision_score, czułość - recall_score,
#miara F1, czyli średnia harmoniczna z czułości i precyzji - f1_score
#funkcja licząca powyższe te miary
def fit_classifier(alg, X_ucz, X_test, y_ucz, y_test):
    alg.fit(X_ucz, y_ucz)
    y_pred_ucz = alg.predict(X_ucz)
    y_pred_test = alg.predict(X_test)
    return {
        "ACC_ucz": sklearn.metrics.accuracy_score(y_pred_ucz, y_ucz),
        "ACC_test": sklearn.metrics.accuracy_score(y_pred_test, y_test),
        "P_ucz":   sklearn.metrics.precision_score(y_pred_ucz, y_ucz),
        "P_test":   sklearn.metrics.precision_score(y_pred_test, y_test),
        "R_ucz":   sklearn.metrics.recall_score(y_pred_ucz, y_ucz),
        "R_test":   sklearn.metrics.recall_score(y_pred_test, y_test),
        "F1_ucz":  sklearn.metrics.f1_score(y_pred_ucz, y_ucz),
        "F1_test":  sklearn.metrics.f1_score(y_pred_test, y_test)
    }
results = pd.DataFrame({'knn_02': fit_classifier(knn, X_ucz, X_test, yk_ucz, yk_test)}).T



results = results.append(pd.DataFrame({'knn_norm_l2_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)

results = results.append(pd.DataFrame({'knn_norm_l1_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)

from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
#niestety, dla tej ramki wychodzily wyniki z weight:distance, ktore byly przeuczone
#ta ramka zostala zastosowana do badania dla cv=5 i cv=10
search_grid = [
  {
    'weights': ['uniform', 'distance'],
    'p': [1, 2],
    'n_neighbors': [5,6,7,8,9,10,11,12,13,14,15]
    }
  ]
#w tej ramce wyniki moze nie byly najlepsze, ale za to nie byly tez przeuczone
#ta ramka byla stosowana w badaniu dla cv=10
# search_grid = [
#   {
#     'p': [1, 2],
#     'n_neighbors': [5,6,7,8,9,10,11,12,13,14,15]
#     }
#   ]


scorer = {'auc': 'accuracy', 'f1': 'f1', 'prec': 'precision', 'rec': 'recall'}
# optymalizacja modelu dla wektorow l2 - zakomentowana poniewaz trwa, a wyniki sa juz zastosowane, podane ponizej i zapisane w pliku
search_func2 = GridSearchCV(estimator=sklearn.neighbors.KNeighborsClassifier(), 
                            param_grid=search_grid,
                            scoring=scorer,
                            n_jobs=-1, iid=False, refit='auc', cv=5)
search_func2.fit(X_norm_l2, yk)
print (search_func2.best_estimator_)
print (search_func2.best_params_)
print (search_func2.best_score_)
all_results = pd.DataFrame(search_func2.cv_results_)                    
all_results.to_csv("opt_model_l2.csv")
#NASZ WYNIK GRID SEARCHU Z WEIGHT
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=14, p=2,
#                      weights='distance')
# {'n_neighbors': 14, 'p': 2, 'weights': 'distance'}
# 0.8360039806771298
#NASZ WYNIK GRID SEARCHU BEZ WEIGHT
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=15, p=2,
#                      weights='uniform')
# {'n_neighbors': 15, 'p': 2}
# 0.8334111500425021
#NASZ WYNIK GRID SEARCHU Z WEIGHT DLA CV=5
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=13, p=2,
#                      weights='distance')
# {'n_neighbors': 13, 'p': 2, 'weights': 'distance'}
# 0.8351331893448524

#optymalizacja modelu dla wektorow l1 - zakomentowana poniewaz dlugo trwa, a wyniki sa juz zastosowane, podane ponizej i zapisane w pliku
search_func1 = GridSearchCV(estimator=sklearn.neighbors.KNeighborsClassifier(),
                            param_grid=search_grid,
                            scoring=scorer,
                            n_jobs=-1, iid=False, refit='auc', cv=5)
search_func1.fit(X_norm_l1, yk)
print (search_func1.best_estimator_)
print (search_func1.best_params_)
print (search_func1.best_score_)
all_results = pd.DataFrame(search_func1.cv_results_)
all_results.to_csv("opt_model_l1.csv")
#NASZ WYNIK POWYZSZEGO GRID SEARCHU Z WEIGHT
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=13, p=1,
#                      weights='distance')
# {'n_neighbors': 13, 'p': 1, 'weights': 'distance'}
# 0.8391700702838307   
#NASZ WYNIK GRID SEARCHU BEZ WEIGHT
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=8, p=1,
#                      weights='uniform')
# {'n_neighbors': 8, 'p': 1}
# 0.8368662533949784
#NASZ WYNIK GRID SEARCHU Z WEIGHT DLA CV=5
# KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#                      metric_params=None, n_jobs=None, n_neighbors=10, p=1,
#                      weights='distance')
# {'n_neighbors': 10, 'p': 1, 'weights': 'distance'}
# 0.838012958963283

results = results.append(pd.DataFrame({'knn_norm_l2_opt_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 14, p=2, weights = "distance"),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)

results = results.append(pd.DataFrame({'knn_norm_l1_opt_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 13, p=1, weights = "distance"),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l2_opt_bez_weight_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 15, p=2),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_opt_bez_weight_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 8, p=1),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l2_opt_cv5_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 13, p=2, weights = "distance"),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_opt_cv5_02': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 10, p=1, weights = "distance"),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)

# TESTUJEMY DLA PODZIALU DANYCH NA 30:70 zamiast 20:80
idx_ucz, idx_test = sklearn.model_selection.train_test_split(np.arange(X.shape[0]), test_size=0.3, random_state=12345)
X_ucz, X_test = X.iloc[idx_ucz, :], X.iloc[idx_test, :]
y_ucz, y_test = y[idx_ucz], y[idx_test]
yk_ucz, yk_test = yk[idx_ucz], yk[idx_test]
#normalizujemy wektory l2
from sklearn import preprocessing
X_norm_l2_ucz = preprocessing.normalize(X_ucz)
X_norm_l2_test = preprocessing.normalize(X_test)
X_norm_l2 = preprocessing.normalize(X)
#normalizujemy wektory l1
X_norm_l1_ucz = preprocessing.normalize(X_ucz, norm = "l1")
X_norm_l1_test = preprocessing.normalize(X_test, norm = "l1")
X_norm_l1 = preprocessing.normalize(X, norm = "l1")
#Dodajemy do pliku nasze wyniki z modelami dla podzialu 30:70
print("Polowa drogi")
results = results.append(pd.DataFrame({'knn_norm_l2_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l2_opt_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 14, p=2, weights = "distance"),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_opt_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 13, p=1, weights = "distance"),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l2_opt_bez_weight_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 15, p=2),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_opt_bez_weight_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 8, p=1),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l2_opt_cv5_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 13, p=2, weights = "distance"),
                                                              X_norm_l2_ucz,
                                                              X_norm_l2_test,
                                                              yk_ucz,
                                                              yk_test)}).T)
results = results.append(pd.DataFrame({'knn_norm_l1_opt_cv5_03': fit_classifier(sklearn.neighbors.KNeighborsClassifier(n_neighbors = 10, p=1, weights = "distance"),
                                                              X_norm_l1_ucz,
                                                              X_norm_l1_test,
                                                              yk_ucz,
                                                              yk_test)}).T)


results.to_csv("wyniki.csv")

#ZASTOSOWANIE LASU LOSOWEGO W CELU ZNALEZIENIA NAJLEPSZYCH TAGOW DO GIER
import sklearn.ensemble
las = sklearn.ensemble.RandomForestClassifier(random_state=123)
#parametry algorytmu
las.fit(X_ucz, yk_ucz)

pd.Series(las.feature_importances_, index = X.columns[0:320]).sort_values(ascending=False).to_csv("istotnosc.csv")